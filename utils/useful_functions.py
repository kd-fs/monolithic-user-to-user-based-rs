""" 
@author: Darlin KUAJO
Ce module contient les fonctions utiles à l'implémentation de MU2UBRS
"""

import os
import nltk
import string
import pandas as pd
from tqdm import tqdm
from math import sqrt, pow
from IPython.core.display import HTML

CSS = """
<style>
h1, th{
    text-align:center;
}
td{
    text-align:justify;
}
.dataframe{
    border-collapse:collapse;
    margin: 0 auto;
}
th, td{
    padding: 10px;
}
</style>
\n
"""

def getcategories(asin, metadata):
    """Cette fonction retoune la liste des catégories du produit identifié par asin
    
    asin : str
    metadata : pd.DataFrame
    
    Valeur de retour : list(str)
    """
    return metadata[metadata['asin'] == asin].reset_index(inplace=False, drop = True).iloc[0]['categories']

def getItemByIndex(index, metadata):
    """ Cette fonction permet de retourner l'identifiant d'un produit à partir de son index
    
    metadata : pd.DataFrame
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    return metadata.iloc[index]['asin']

def getUserByIndex(reviewerID, evaluation_matrix):
    """ Cette fonction permet de retourner l'identifiant d'un utilisateur à partir de son index
    
    evaluation_matrix : dict
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    users = list(evaluation_matrix.keys())
    return users[index]

def imUrl_to_image_html_width100(imUrl):
    """Cette fonction permet de convertir l'adressse url de l'image d'un produit en
    balise HTML nécessaire pour afficher une image cliquable avec une largeur de 100px
    
    imUrl : str
    
    Valeur de retour : str
    """
    
    return '<a href="' + imUrl + '">' + '<img src="'+ imUrl + '" width="100px" >' + '</a>'

def visualisation_select_product(k, asin, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné.
    
    k : int
    asin : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame nécessaire pour la visualisation du produit choisi
    print("[INFO] Génération d'un affichage du produit sélectionné ...")
    item_df = metadata[metadata['asin'] == asin]
    item_df.reset_index(inplace=True, drop=True)
    item_df = item_df.loc[:,['asin', 'description', 'categories', 'imUrl']]
    item_df.rename(columns={'imUrl' : 'image'}, inplace=True)
    code_html = item_df.to_html(escape=False,index=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Création du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'w', encoding='UTF-8') as fichier:
        fichier.write(CSS)
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>Produit sélectionné</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(item_df.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))

def visualisation_recommended_products(k, recommendations_list, score_name, list_title, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné et de la liste de recommandation
    
    k : int
    recommendations_list : list(flozt, str)
    name_score : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame qui présentera la liste de recommandation
    print("[INFO] Génération d'un affichage de la liste de recommandation ...")
    score = []
    item = []
    for i in range(len(recommendations_list)):
        score.append(recommendations_list[i][0])
        item.append(recommendations_list[i][1])
    recommended_products = pd.DataFrame({"asin" : item, score_name : score})
    recommended_products = pd.merge(recommended_products, metadata, how='inner', on='asin')
    recommended_products = recommended_products.rename(columns={'imUrl':'image'})
    recommended_products = recommended_products.loc[:, ['asin', score_name, 'description', 'categories','image']]
    code_html = recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Mise à jour du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'a', encoding='UTF-8') as fichier:
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>""" + list_title + """</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))

def sim_pearson(evaluation_matrix, user1, user2):
    """Cette fonction renvoie le coefficient de corrélation de Pearson pour les utilisateurs user1 et user2
    
    evaluation_matrix : dict
    user1 : str
    user2 : str
    
    Valeur de retour : float
    """
    
    # Récupération des items évalués par user1 et user2
    common_items = {}
    for item in evaluation_matrix[user1]:
        if item in evaluation_matrix[user2]:
            common_items[item] = 1
            
    # Détermination de la longueur des vecteurs d'évaluation
    n = len(common_items)
    
    if n == 0:  # Si la longueur  des vecteurs d'évaluation est 0 alors pas de similitude entre user1 et user2
        return 0
    
    # Addition de toutes les notes
    sum1 = sum([evaluation_matrix[user1][item] for item in common_items])
    sum2 = sum([evaluation_matrix[user2][item] for item in common_items])
        
    # Addition des carrés de toutes les notes
    sum1Sq = sum([pow(evaluation_matrix[user1][item],2) for item in common_items])
    sum2Sq = sum([pow(evaluation_matrix[user2][item],2) for item in common_items])
        
    # Addition des produits des notes
    pSum = sum([evaluation_matrix[user1][item] * evaluation_matrix[user2][item] for item in common_items])
        
    # Calcul du coefficient de corrélation de pearson
    num = pSum - ((sum1 * sum2) / n)
    den = sqrt((sum1Sq - (pow(sum1,2) / n)) * (sum2Sq - (pow(sum2,2) / n)))
    
    if den == 0: # Si le dénominateur du coefficient de correlation de pearson est 0 alors pas de similitude entre user1 et user2
        return 0
    pearcoef = num / den
        
    return pearcoef

def top_n_users(user, n, evaluation_matrix, similarity_mesure):
    """Cette fonction renvoie les n premiers utilisateurs les plus similaires à user,
    selon l'ordre décroissant des scores de similarité des utilisateurs par rapport à user
    
    user = str
    n : int
    evaluation_matrix : dict
    similarity_mesure : function
   
    n > 0
    
    Valeur de retour : list((float, str))
    """
    print("[INFO] Détermination des "+ str(n) + " premiers clients les plus similaires au client ...")
        
    # Détermination des scores de similarité entre tous les autres utilisateurs et user
    similarity_scores = [(similarity_mesure(evaluation_matrix, user, other_user), other_user) for other_user in evaluation_matrix if other_user != user]
    
    # Trie des utilisateurs par ordre décroissant des scores de similarité
    similarity_scores.sort(reverse=True)
    
    return similarity_scores[0:n]

def top_k_recommendation(user, asin, similar_users, k, evaluation_matrix,  metadata, similarity_mesure):
    """Cette fonction renvoie la liste des k premiers produits pouvant être recommandés à user,
    selon l'ordre décroissant des évaluations moyennes des produits pesées par la similarité entre utilisateurs.
    
    La taille de la liste renvoyée est <= k
    
    user : str
    asin : str
    similar_users : list((float, str))
    k : int
    evaluation_matrix : dict
    data : pd.DataFrame
    similarity_mesure : function
    
    n > 0
        
     Valeur de retour : list(IPython.core.display.HTML, IPython.core.display.HTML)
    """
    
    totals = {}
    simSums = {}
    categories = getcategories(asin, metadata)
    
    print("[INFO] Calcul des évaluations moyennes pesées par la similarité entre utilisateurs ...")
    for similarity_score, other_user in similar_users:
        # On ignore les utilisateurs dont le score de similarité à user est <= 0
        if similarity_score <= 0:
            continue
        for item in evaluation_matrix[other_user]:
            # On ne considère que les produits que user n'a pas encore évalué
            if item not in evaluation_matrix[user]:
                # Somme des produits entre les scores de similarité et les notes
                totals.setdefault(item,0)
                totals[item] += evaluation_matrix[other_user][item] * similarity_score
                # Ssomme des scores de similarité
                simSums.setdefault(item,0)
                simSums[item] += similarity_score
    
    # Création de la liste de recommandation
    if len(totals.keys()) == 0:
        recommendations_list = []
    recommendations_list = [(total/simSums[item],item) for item,total in totals.items()]
    
    # Trions cette liste par ordre décroissant des évaluations moyennes des produits pesées par les scores de similarités
    print("[INFO] Tri de la liste par ordre décroissaant des évaluation moyenne ...")
    recommendations_list.sort(reverse=True)
    
    #Extrayons de cette liste tous les produits ayant la meme categorie que le produit sélectionnés
    print("[INFO] Filtrage de la liste en fonction de la cotégorie du produit choisi ...")
    same_product_categories = []
    for mean_eval, item in recommendations_list:
        if len(same_product_categories) != k:
            if categories ==getcategories(item, metadata) and asin != item:
                same_product_categories.append((mean_eval, item)) 
    recommendations_list = same_product_categories
    
    # Extraction des n premiers produits
    if len(recommendations_list) > k:
        recommendations_list = recommendations_list[0:kn]
    
    return (visualisation_select_product(k, asin, metadata),
            visualisation_recommended_products(k, recommendations_list, "mean_evaluation", "Les clients ont également appréciés", metadata)
           )